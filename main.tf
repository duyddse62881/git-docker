terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "8fb8c49d-f030-4e98-8559-8a14d656a649"
  client_id       = "b0de4c40-c317-4f0f-9c8e-c0c4692d3ec3"
  client_secret   = "FYMz-s8S3w2zxIxiT25V_thZqunHnL2s.B"
  tenant_id       = "b3751638-16fe-4e63-bb38-440c61f1bd04"
}




# resource "null_resource" "example"{
#         provisioner "local-exec"{
#            command = "ansible-playbook -i /etc/ansible/hosts /etc/ansible/site.yml --limit linux_servers"
#         }
# }

# Create a resource group
resource "azurerm_resource_group" "rg" {
  name     = "Lab-rg-ddd"
  location = "westus2"
}

#Create Storage Account
module "storage_account" {
  source = "./storage-account"

  storagename = "labsaddd"
  rgname      = azurerm_resource_group.rg.name
  location    = azurerm_resource_group.rg.location
}

# Create virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "LabVnetddd"
  address_space       = ["10.10.0.0/16", "10.16.0.0/16"]
  location            = "westus2"
  resource_group_name = azurerm_resource_group.rg.name
}

# Create subnet linux
resource "azurerm_subnet" "subnet" {
  name                 = "linuxSubnet1ddd"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.10.0.0/16"]
}

# Create subnet window
resource "azurerm_subnet" "subnet1" {
  name                 = "windowSubnet1ddd"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.16.0.0/16"]
}

# Create public IP linux
resource "azurerm_public_ip" "publicip" {
  name                = "linuxPublicIPddd"
  location            = "westus2"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}


# Create public IP window
resource "azurerm_public_ip" "publicip1" {
  name                = "windowTFPublicIPddd"
  location            = "westus2"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}



resource "azurerm_network_security_group" "nsg_linux" {
  name                = "customNetworkSecurityGroupddd"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  security_rule {
    name                       = "Custom-Linux-Rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-Ping-Rule"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Terraform Demo"
  }
}

resource "azurerm_network_security_group" "nsg_win" {
  name                = "customNetworkSecurityGroup-Winddd"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "Custom-Windows-Rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_ranges    = [5985, 22, 3389]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-Ping-Rule"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Terraform Demo"
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "linux" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg_linux.id
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "win" {
  network_interface_id      = azurerm_network_interface.nic1.id
  network_security_group_id = azurerm_network_security_group.nsg_win.id
}



# Create network interface
resource "azurerm_network_interface" "nic" {
  name                = "linuxLabNICddd"
  location            = "westus2"
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "linuxNICConfg"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip.id
  }
}

# Create network interface
resource "azurerm_network_interface" "nic1" {
  name                = "windowLabNICddd"
  location            = "westus2"
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "windowNICConfg"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip1.id
  }
}

# Create a Linux virtual machine
resource "azurerm_virtual_machine" "vm" {
  name                  = "linuxVM1ddd"
  location              = "westus2"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "linuxOsDisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "linuxVM1"
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}

# Create a Window virtual machine
resource "azurerm_virtual_machine" "vm1" {
  name                  = "windowVM3ddd"
  location              = "westus2"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic1.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "windowOsDisk3"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  os_profile {
    computer_name  = "windowVM3"
    admin_username = var.admin_username
    admin_password = var.admin_password
    custom_data    = file("./files/winrm.ps1")
  }

  os_profile_windows_config {
    provision_vm_agent = true
    winrm {
      protocol = "http"
    }
    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    }

    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = file("./files/FirstLogonCommands.xml")
    }
  }

  connection {
    host     = azurerm_public_ip.publicip1.ip_address
    port     = 5985
    https    = false
    type     = "winrm"
    timeout  = "2m"
    user     = var.admin_username
    password = var.admin_password
  }

  provisioner "file" {
    source      = "files/config.ps1"
    destination = "c:/terraform/config.ps1"
  }


  provisioner "remote-exec" {
    on_failure = continue
    inline = [
      "powershell.exe -ExecutionPolicy Bypass -File C:/terraform/config.ps1",
    ]
  }

  # provisioner "file" {
  #   source      = "/Downloads/config.ps1"
  #   destination = "C:/Desktop"
  # }


  # provisioner "remote-exec" {
  #   inline = [
  #     "powershell.exe -File C:\\Desktop\\config.ps1"
  #   ]
  # }
  provisioner "local-exec" {
    command = "terraform output -json > ./ansible/ip.json;export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i ./ansible/hosts ./ansible/site.yml"
  }
}

# data "azurerm_public_ip" "ip" {
#   name                = azurerm_public_ip.publicip.name
#   resource_group_name = azurerm_virtual_machine.vm.resource_group_name
#   depends_on          = [azurerm_virtual_machine.vm]
# }

# data "azurerm_public_ip" "ip1" {
#   name                = azurerm_public_ip.publicip1.name
#   resource_group_name = azurerm_virtual_machine.vm1.resource_group_name
#   depends_on          = [azurerm_virtual_machine.vm1]
# }

output "public_ip_address_unix" {
  value = azurerm_public_ip.publicip.ip_address
}

output "public_ip_address_win" {
  value = azurerm_public_ip.publicip1.ip_address
}
