variable "admin_username" {
    description = "Administrator user name for virtual machine"
}

variable "admin_password" {
    description = "Password must meet Azure complexity requirements"
}

